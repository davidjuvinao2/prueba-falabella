//
//  LoginViewController.swift
//  FalabellaFinanciero
//
//  Created by David on 20-01-21.
//

import UIKit
import CryptoKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    @IBAction func login(_ sender: Any) {
        let email: String? = emailTextField.text!
        let pass: String? = passwordTextField.text!
        let validLogin: Bool = getLogin(email: email!, pass: pass!)
        
        print("boton click "+email!)
        print("boton click "+validLogin.description)
        
        if (validLogin) {
            dismiss(animated: true, completion: nil)
            let mainViewController = MainViewController()
            navigationController?.setViewControllers([mainViewController], animated: true)
        } else {
            let alert = UIAlertController(title: "Error", message: "Correo y/o contraseña invalido", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{ (action: UIAlertAction!) in
            }))
            present(alert, animated: true, completion: nil)
            
        }
         
    }
    
    @IBAction func signup(_ sender: Any) {
        let email: String? = emailTextField.text!
        
        print("boton click "+email!)
        dismiss(animated: true, completion: nil)
        let registerViewController = RegisterViewController()
        navigationController?.setViewControllers([registerViewController], animated: true)
    }

    
    func getLogin(email: String?, pass:String?) -> Bool {
        print("getLogin "+email!)
        
        let data = pass?.data(using: .utf8)
        let hashed = SHA512.hash(data: data!)

        
        var islogin: Bool = false
        var users = [User]()
        let preferences = UserDefaults.standard

        
        if let data = preferences.data(forKey: "users") {
            do {
                let decoder = JSONDecoder()
                users = try decoder.decode([User].self, from: data)
                
                if users.count > 0 {

                    for user in users {
                        print(user.email + " " + hashed.description)
                        if (user.email == email && user.password == hashed.description) {
                            preferences.set(user.name, forKey: "name")
                            preferences.set(user.lastname, forKey: "lastname")
                            preferences.set(user.email, forKey: "email")
                            didSave(preferences: preferences)
                            islogin = true
                            break
                        }
                    }
                }
                
            } catch {
                print("Error getting UsersRegister \(error)")
            }
        }

        return islogin
    }
    
    func hashCode(clave:String) -> String? {
        guard let data = clave.data(using: .utf8) else {
            return nil
        }
        let hashed = SHA512.hash(data: data)
        return hashed.description
    }
    
    func didSave(preferences: UserDefaults){
        let didSave = preferences.synchronize()
        if !didSave{
            print("Preferences could not be saved!")
        }
    }
}
