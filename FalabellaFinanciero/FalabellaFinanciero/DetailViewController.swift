//
//  DetailViewController.swift
//  FalabellaFinanciero
//
//  Created by David on 25-01-21.
//

import UIKit

class DetailViewController: UIViewController {

    var indicator = Indicator()

    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var unit_measure: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("Prueba \(indicator.nombre ?? "")")
        self.code?.text = indicator.codigo
        self.name?.text = indicator.nombre ?? ""
        self.unit_measure?.text = indicator.unidad_medida
        self.date?.text = indicator.fecha
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
