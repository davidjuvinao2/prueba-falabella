//
//  MainViewController.swift
//  FalabellaFinanciero
//
//  Created by David on 21-01-21.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var indicatorsTable: UITableView!
    var response: Response!
    var indicators: [Indicator] = []
    var filterIndicators: [Indicator] = []
    
    var searchController : UISearchController!
    var resultsController = UITableViewController()

    override func viewDidLoad() {
        super.viewDidLoad()


        let name = getNameUserLogin()
        welcomeLabel.text = "Welcome \(name)!"
        
        indicatorsTable.delegate = self
        indicatorsTable.dataSource = self
        
        self.creatingSearhBar()
        self.tableSettings()

        let service = Service(baseUrl: "https://www.mindicador.cl/")
        service.getAllIndicators(endPoint: "api")
        service.completionHandler { [weak self] (response, status, message) in
            if status {
                guard let self = self else {return}
                guard let _indicators = response else {return}
                self.response = _indicators
                
                self.indicators = self.response.getIndicators()
                
                self.indicatorsTable.reloadData()
            }
        }

    }
    
    func getNameUserLogin() -> String {
        var name: String!
        let preferences = UserDefaults.standard
        name = preferences.string(forKey: "name")

        return name
    }
    

    func creatingSearhBar() {
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.indicatorsTable.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self
    }

    func tableSettings() {
        self.resultsController.tableView.dataSource = self
        self.resultsController.tableView.delegate = self
        self.resultsController.tableView.rowHeight = 204
        self.resultsController.tableView.separatorStyle = .none

    }
    
    
    @IBAction func logout(_ sender: Any) {
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: "name")
        let loginViewController = LoginViewController()
        navigationController?.setViewControllers([loginViewController], animated: true)
    }

}


extension MainViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var indicator = Indicator()
        if tableView == self.indicatorsTable {
            indicator = indicators[indexPath.row]
        }else{
            indicator = filterIndicators[indexPath.row]
        }

        dismiss(animated: true, completion: nil)
        let detailViewController = DetailViewController(nibName: "DetailViewController", bundle: nil)
        detailViewController.indicator = indicator

        navigationController?.popToRootViewController(animated: true)
        navigationController?.pushViewController(detailViewController, animated: true)

    }
    
}


extension MainViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return indicators.count
        if tableView == self.indicatorsTable {
            return indicators.count
        }
        else {
            return filterIndicators.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "indicatorcell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "indicatorcell")
        }
        
        
        var indicator = Indicator()
        if tableView == self.indicatorsTable {
            indicator = indicators[indexPath.row]
        }else{
            indicator = filterIndicators[indexPath.row]
        }

        cell?.textLabel?.text = (indicator.nombre ?? "")
        cell?.detailTextLabel?.text = String(format: "%f", indicator.valor)

        return cell!
    }
}


extension MainViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filterIndicators = self.indicators.filter { (indicator: Indicator) -> Bool in
            if indicator.codigo!.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
                return true
            } else{
                return false
            }
        }
        
        self.resultsController.tableView.reloadData()
    }

}
