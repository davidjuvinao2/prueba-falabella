//
//  RegisterViewController.swift
//  FalabellaFinanciero
//
//  Created by David on 21-01-21.
//

import UIKit
import CryptoKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func enCrypt(value: String!) -> String {
        let data = value?.data(using: .utf8)
        let hashed = SHA512.hash(data: data!)
        return hashed.description
    }
    
    
    @IBAction func register(_ sender: Any) {

        let name: String? =  nameTextField.text!
        let lastname: String? = lastnameTextField.text!
        let email: String? = emailTextField.text!
        let password: String? = enCrypt(value: passwordTextField.text!)

        let user = User(name: name!, lastname: lastname!, email: email!, password: password!)
        
        print("name: \(name!) apellido: \(lastname!) email: \(email!) contraseña: \(password!) ")
        
        var usersRegisters = getUsersRegister()
        
        print("->"+usersRegisters.description)
        
        if (usersRegisters.count == 0) {
            usersRegisters = [User]()
            usersRegisters.append(user)
        } else {
            usersRegisters.append(user)
        }
        print(usersRegisters.description)
        print(usersRegisters.count)
        
        do {

            let encoder = JSONEncoder()
            let data = try encoder.encode(usersRegisters)
            
            let preferences = UserDefaults.standard
            preferences.set(data, forKey: "users")

            if didSave(preferences: preferences) {
                dismiss(animated: true, completion: nil)
                let loginViewController = LoginViewController()
                navigationController?.pushViewController(loginViewController, animated: true)
            }
            
        } catch {
            print("Error encoding usersRegister \(error)")
        }
        
    }
    
    
    func getUsersRegister() -> [User] {
        var users = [User]()
        let preferences = UserDefaults.standard
        if let data = preferences.data(forKey: "users") {
            do {
                let decoder = JSONDecoder()
                users = try decoder.decode([User].self, from: data)
            } catch {
                print("Error getting UsersRegister \(error)")
            }
        }
        
        return users
    }

    
    func didSave(preferences: UserDefaults) -> Bool {
        var isSaved = true
        let didSave = preferences.synchronize()
        if !didSave{
            isSaved = false
            print("Preferences could not be saved!")
        }
        return isSaved
    }
    
}
