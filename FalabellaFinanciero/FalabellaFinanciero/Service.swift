//
//  Service.swift
//  FalabellaFinanciero
//
//  Created by David on 22-01-21.
//

import Foundation
import Alamofire

class Service {

    fileprivate var baseUrl = ""
    typealias indicatorsCallBack = (_ response:Response?, _ status: Bool, _ message:String) -> Void
    var callBack:indicatorsCallBack?
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func getAllIndicators(endPoint:String) {
        AF.request(self.baseUrl + endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).responseData { (response) in

            guard let data = response.data else {return}
            do {
   
                let indicators = try JSONDecoder().decode(Response.self, from: data)
                print("Indicadores: \(indicators)")

                self.callBack?(indicators, true,"")
            } catch {
                print("Error obteniendo los indicadores \(error)")
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }

    
    func completionHandler(callBack: @escaping indicatorsCallBack) {
        self.callBack = callBack
    }
}
