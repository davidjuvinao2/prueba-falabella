//
//  User.swift
//  FalabellaFinanciero
//
//  Created by David on 21-01-21.
//

import UIKit

struct User: Codable /*: NSObject, NSCoding */{

    var name: String
    var lastname: String
    var email: String
    var password: String


    init(name: String, lastname: String, email: String, password: String) {
        self.name = name
        self.lastname = lastname
        self.email = email
        self.password = password
    }
  /*
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let lastname = aDecoder.decodeObject(forKey: "lastname") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let password = aDecoder.decodeObject(forKey: "password") as! String
        self.init(name: name, lastname: lastname, email: email, password: password)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(lastname, forKey: "lastname")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")
    }*/
}
