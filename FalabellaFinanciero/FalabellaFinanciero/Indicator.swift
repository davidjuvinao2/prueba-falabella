//
//  Indicator.swift
//  FalabellaFinanciero
//
//  Created by David on 24-01-21.
//

import Foundation

struct Response : Decodable {
    var version: String?
    var autor: String?
    var fecha: String?
    var uf: Indicator
    var ivp: Indicator
    var dolar: Indicator
    var dolar_intercambio: Indicator
    var euro: Indicator
    var ipc: Indicator
    var utm: Indicator
    var imacec: Indicator
    var tpm: Indicator
    var libra_cobre: Indicator
    var tasa_desempleo: Indicator
    var bitcoin: Indicator
    
    enum CodingKeys: String, CodingKey {
        case version = "version"
        case autor = "autor"
        case fecha = "fecha"
        case uf
        case ivp
        case dolar
        case dolar_intercambio
        case euro
        case ipc
        case utm
        case imacec
        case tpm
        case libra_cobre
        case tasa_desempleo
        case bitcoin
    }
    
    func getIndicators() -> [Indicator] {
        
        var indicators = [Indicator]()
        
        indicators.append(uf)
        indicators.append(ivp)
        indicators.append(dolar)
        indicators.append(dolar_intercambio)
        indicators.append(euro)
        indicators.append(ipc)
        indicators.append(utm)
        indicators.append(imacec)
        indicators.append(tpm)
        indicators.append(libra_cobre)
        indicators.append(tasa_desempleo)
        indicators.append(bitcoin)
        
        return indicators
    }

}

struct Indicator: Decodable {
    var codigo: String?
    var nombre: String?
    var unidad_medida: String?
    var fecha: String?
    var valor: Double = 0
    
    
    enum CodingKeys: String, CodingKey {
        case codigo = "codigo"
        case nombre = "nombre"
        case unidad_medida = "unidad_medida"
        case fecha = "fecha"
        case valor = "valor"
    }
    
}






